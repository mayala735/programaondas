import sys
import matplotlib as mpl
mpl.use("Qt5Agg")
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from numpy import sin, linspace, exp, sqrt
from scipy import pi
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FC
from matplotlib.figure import Figure
from matplotlib import pyplot as plt
import time

permi0 = 8.84/1000000000000
perme0 = 1.25/1000000

class MplCanvas(FC):
    def __init__(
        self,
        parent = None,
        width = 8,
        height = 6.5,
        lamda = 555,
        permi = 1,
        perme = 1,
        cond = 1,
        ampl = 1,
        #Agrego un parametro para ajustar la cantidad de ciclos a mostrar
        per = 10,
        ):
        fig = Figure(figsize = (width, height ), dpi=200)
        self.ax = fig.add_subplot(111)
        self.ax.set_facecolor('#D3D3D3')
        #self.ax.hold(False)            
        #self.ax.hold ya no se utiliza desde matplotlib3
        #hay que llamar self.ax.cla() cada vez que querramos actualizar el grafico
        #   ver linea en onSomethingChanged
        
        FC.__init__(self, fig )
        FC.setSizePolicy(
            self,
            QSizePolicy.Expanding,
            QSizePolicy.Expanding
            )
        FC.updateGeometry(self )
        MplCanvas.drawPlot(self, lamda, permi, perme, cond, ampl, per)
        
    def drawPlot(self, lamda, permi, perme, cond, ampl, per ):
        #En estas funciones calculo paso a paso los valores de
        #alpha y beta, que me dan atenuacion y desfasaje de la señal
        
        frec = 300000000/lamda
        omega = 2*3.14*frec

        cond = cond/1000000
        
        paso1 = sqrt(1+(cond/(omega*permi*permi0*omega*permi*permi0)))
        paso2a = sqrt(paso1-1)
        paso2b = sqrt(paso1+1)
        paso3 = 2*3.14/lamda*sqrt(perme*permi)/sqrt(2)
        
        alpha = paso3 * paso2a
        beta = paso3 * paso2b
    
        self.x = linspace (0 ,per*0.7 ,1024)
        self.y = ampl * sin(2 * pi * self.x/( lamda /1000) + beta) * exp(-alpha * self.x)
        
        if int(lamda) > 620:
            colour_hex = 0xFF00FF
            print(hex(colour_hex))
            colour_hex = colour_hex - int(lamda)*216

            colour = '#' + str(hex(colour_hex))[2:]
            print(colour)
            
        elif int(lamda) > 590:
            colour = 'b'

        elif int(lamda) > 570:
            colour = 'g'

        elif int(lamda) > 495:
            colour = 'y'

        elif int(lamda) > 450:
            colour = 'orange'

        else: 
            colour = 'red'

           
        self.ax.plot(self.x,self.y,color=colour)
        self.ax.set_ylim(-2,2)
        self.ax.set_xticklabels ([])
        self.ax.set_yticklabels ([])
        #self.ax.secondary_xaxis('bottom')
        #self.ax.secondary_yaxis('left')
        self.ax.set_title('Funcion Campo Electrico E')
        self.draw ()

class MainApp(QMainWindow ):
    def __init__(self):
        QMainWindow.__init__(self)
        
        self.setAttribute(Qt.WA_DeleteOnClose)
        self.setWindowTitle("Ondas")

        self.main_widget = QWidget(self)
        
        
        
        self.loAmpl = QVBoxLayout()
        self.lblAmpl = QLabel("Amplitud", self)
        self.sldAmpl = QSlider(Qt. Horizontal )
        self.sldAmpl.setMinimum(10)
        self.sldAmpl.setMaximum(200)
        self.sldAmpl.setValue(100)
        self.sldAmpl.setTickPosition(QSlider.TicksBelow )
        self.sldAmpl.setTickInterval(1)
        self.edtAmpl = QLineEdit(self)
        self.edtAmpl.setMaxLength(5)
        self.loAmpl.addWidget(self.lblAmpl )
        self.loAmpl.addSpacing(3)
        self.loAmpl.addWidget(self.sldAmpl )
        self.loAmpl.addSpacing(3)
        self.loAmpl.addWidget(self.edtAmpl )

        
        self.loLambda = QVBoxLayout()
        self.lblLambda = QLabel("Longitud de onda [nm]", self)
        self.sldLambda = QSlider(Qt.Horizontal )
        self.sldLambda.setMinimum(400)
        self.sldLambda.setMaximum(700)
        self.sldLambda.setValue(700)
        self.sldLambda.setTickPosition(QSlider.TicksBelow)
        self.sldLambda.setTickInterval(5)
        self.edtLambda = QLineEdit(self)
        self.edtLambda.setMaxLength(5)
        self.loLambda.addWidget(self.lblLambda )
        self.loLambda.addSpacing(3)
        self.loLambda.addWidget(self.sldLambda )
        self.loLambda.addSpacing(3)
        self.loLambda.addWidget(self.edtLambda )


        self.loPer = QVBoxLayout()
        self.lblPer = QLabel("Ciclos", self)
        self.sldPer = QSlider(Qt. Horizontal )
        self.sldPer.setMinimum(1)
        self.sldPer.setMaximum(10)
        self.sldPer.setValue(1)
        self.sldPer.setTickPosition(QSlider.TicksBelow )
        self.sldPer.setTickInterval(1)
        self.edtPer = QLineEdit(self)
        self.edtPer.setMaxLength(5)
        self.loPer.addWidget(self.lblPer )
        self.loPer.addSpacing(3)
        self.loPer.addWidget(self.sldPer )
        self.loPer.addSpacing(3)
        self.loPer.addWidget(self.edtPer )

        
        self.loPerme = QVBoxLayout()
        self.lblPerme = QLabel("Permeabilidad relativa %", self)
        self.sldPerme = QSlider(Qt. Horizontal )
        self.sldPerme.setMinimum(100)
        self.sldPerme.setMaximum(500)
        self.sldPerme.setValue(1)
        self.sldPerme.setTickPosition(QSlider.TicksBelow )
        self.sldPerme.setTickInterval(1)
        self.edtPerme = QLineEdit(self)
        self.edtPerme.setMaxLength(5)
        self.loPerme.addWidget(self.lblPerme )
        self.loPerme.addSpacing(3)
        self.loPerme.addWidget(self.sldPerme )
        self.loPerme.addSpacing(3)
        self.loPerme.addWidget(self.edtPerme )
        


        self.loPermi = QVBoxLayout()
        self.lblPermi = QLabel("Permitividad relativa %", self)
        self.sldPermi = QSlider(Qt. Horizontal )
        self.sldPermi.setMinimum(100)
        self.sldPermi.setMaximum(500)
        self.sldPermi.setValue(1)
        self.sldPermi.setTickPosition(QSlider.TicksBelow )
        self.sldPermi.setTickInterval(1)
        self.edtPermi = QLineEdit(self)
        self.edtPermi.setMaxLength(5)
        self.loPermi.addWidget(self.lblPermi )
        self.loPermi.addSpacing(3)
        self.loPermi.addWidget(self.sldPermi )
        self.loPermi.addSpacing(3)
        self.loPermi.addWidget(self.edtPermi )


        self.loCond = QVBoxLayout()
        self.lblCond = QLabel("Conductividad [uOhm]", self)
        self.sldCond = QSlider(Qt. Horizontal )
        self.sldCond.setMinimum(0)
        self.sldCond.setMaximum(1000)
        self.sldCond.setValue(0)
        self.sldCond.setTickPosition(QSlider.TicksBelow )
        self.sldCond.setTickInterval(1)
        self.edtCond = QLineEdit(self)
        self.edtCond.setMaxLength(5)
        self.loCond.addWidget(self.lblCond )
        self.loCond.addSpacing(3)
        self.loCond.addWidget(self.sldCond )
        self.loCond.addSpacing(3)
        self.loCond.addWidget(self.edtCond )



        self.loWaveParams = QHBoxLayout()
        self.loWaveParams.addLayout(self.loAmpl )
        self.loWaveParams.addStretch()
        self.loWaveParams.addLayout(self.loLambda )
        self.loWaveParams.addStretch()
        self.loWaveParams.addLayout(self.loPer )
        
        
        
        self.loWaveParams2 = QHBoxLayout()
        self.loWaveParams2.addLayout(self.loPerme )
        self.loWaveParams2.addStretch()
        self.loWaveParams2.addLayout(self.loPermi )
        self.loWaveParams2.addStretch()
        self.loWaveParams2.addLayout(self.loCond )
        


        lamda = self.sldLambda.value()
        permi = self.sldPermi.value()/100
        perme = self.sldPerme.value()/100
        cond = self.sldCond.value()
        ampl = self.sldAmpl.value()/100
        per = self.sldPer.value()
        self.edtLambda.setText(str(lamda))
        self.edtPermi.setText(str(permi))
        self.edtPerme.setText(str(perme))
        self.edtCond.setText(str(cond))
        self.edtAmpl.setText(str(ampl))
        self.edtPer.setText(str(per))
        
        self.loCanvas = MplCanvas(
            self.main_widget,
            width = 20,
            height = 16,
            lamda = lamda,
            permi = permi,
            perme = perme,
            cond = cond,
            ampl = ampl,
            per = per
            )
        
        self.main_widget.setFocus()
        self.setCentralWidget(self.main_widget)
        
        self.loMaster = QVBoxLayout (self.main_widget )
        self.loMaster.addLayout (self.loWaveParams )
        self.loMaster.addLayout (self.loWaveParams2 )
        self.loMaster.addWidget (self.loCanvas )
        
        
        self.sldLambda.valueChanged.connect(self.OnLambdaChanged )
        self.sldPerme.valueChanged.connect(self.OnPermeChanged )
        self.sldPermi.valueChanged.connect(self.OnPermiChanged )
        self.sldCond.valueChanged.connect(self.OnCondChanged )
        self.sldAmpl.valueChanged.connect(self.OnAmplChanged )
        self.sldPer.valueChanged.connect(self.OnPerChanged)
        
        
        self.edtLambda.editingFinished.connect(self.OnEdtLambdaChanged )
        self.edtPerme.editingFinished.connect(self.OnEdtPermeChanged )
        self.edtPermi.editingFinished.connect(self.OnEdtPermiChanged )
        self.edtCond.editingFinished.connect(self.OnEdtCondChanged )
        self.edtAmpl.editingFinished.connect(self.OnEdtAmplChanged )
        self.edtPer.editingFinished.connect(self.OnEdtPerChanged)
        




        
    def OnLambdaChanged(self):
        lamda = self.sldLambda.value ()
        self.edtLambda.setText(str(lamda ))
        self.OnSomethingChanged ()
        
    def OnPermeChanged(self):
        perme = self.sldPerme.value()/100
        self.edtPerme.setText(str(perme))
        self.OnSomethingChanged()
        
    def OnPermiChanged(self):
        permi = self.sldPermi.value()/100
        self.edtPermi.setText(str(permi))
        self.OnSomethingChanged()
    
    def OnCondChanged(self):
        cond= self.sldCond.value()
        self.edtCond.setText(str(cond))
        self.OnSomethingChanged()
        
    def OnAmplChanged(self):
        ampl = self.sldAmpl.value()/100
        self.edtAmpl.setText(str(ampl))
        self.OnSomethingChanged()

    def OnPerChanged(self):
        per = self.sldPer.value()
        self.edtPer.setText(str(per))
        self.OnSomethingChanged()
        
        
        
        
    def OnEdtLambdaChanged(self):
        lamda = int(self.edtLambda.text())
        self.sldLambda.setValue(lamda)

    def OnEdtPermeChanged(self):
        phi = self.edtPerme.text ()
        self.sldPerme.setValue(float(phi) * 100)

    def OnEdtPermiChanged(self):
        permi = self.edtPermi.text ()
        self.sldPermi.setValue(float(permi) * 100)

    def OnEdtCondChanged(self):
        cond = self.edtCond.text ()
        self.sldCond.setValue(float(cond))

    def OnEdtAmplChanged(self):
        ampl = self.edtAmpl.text()
        self.sldAmpl.setValue(float(ampl) * 100)

    def OnEdtPerChanged(self):
        per = int(self.edtPer.text())
        self.sldPer.setValue(per)

    def OnSomethingChanged (self):
        #usar ".cla()" para limpiar cada vez el grafico
        self.loCanvas.ax.cla()
        lamda = self.sldLambda.value()
        perme = self.sldPerme.value()/100
        permi = self.sldPermi.value()/100
        cond = self.sldCond.value()
        ampl = self.sldAmpl.value()/100
        per = self.sldPer.value()
        self.ax = linspace(0,per*0.7,1024)
        self.loCanvas.drawPlot(lamda, permi, perme, cond, ampl, per)
        


if __name__ == "__main__":
    app = QApplication(sys.argv)
    miAplicacion = MainApp()
    miAplicacion.show()
    app.exec()