import sys
import matplotlib as mpl
mpl.use("Qt5Agg")
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from scipy import pi, linspace, sin
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FC
from matplotlib.figure import Figure
from matplotlib import pyplot as plt

class MplCanvas(FC):
    def __init__(
        self,
        parent = None,
        width = 8,
        height = 6.5,
        lamda = 555,
        phi = pi/4,
        ampl = 1
        ):
        fig = Figure(figsize = (width, height ))
        self.ax = fig.add_subplot(111)
        #self.ax.hold(False)            
        #self.ax.hold ya no se utiliza desde matplotlib3
        #hay que llamar self.ax.cla() cada vez que querramos actualizar el grafico
        #   ver linea en onSomethingChanged
        
        FC.__init__(self, fig )
        FC.setSizePolicy(
            self,
            QSizePolicy.Expanding,
            QSizePolicy.Expanding
            )
        FC.updateGeometry(self )
        MplCanvas.drawPlot(self, lamda, phi, ampl )
        
    def drawPlot(self, lamda, phi, ampl ):
        x = linspace (0 ,0.7 ,1024)
        y = ampl * sin(2 * pi * x/( lamda /1000) + phi)
        self.ax.plot(x,y,'r')
        self.ax.set_ylim(-2,2)
        self.ax.set_xticklabels ([])
        self.ax.set_yticklabels ([])
        #self.ax.secondary_xaxis('bottom')
        self.draw ()

class MainApp(QMainWindow ):
    def __init__(self):
        QMainWindow.__init__(self)
        
        self.setAttribute(Qt.WA_DeleteOnClose)
        self.setWindowTitle("Ondas")
        
        self.main_widget = QWidget(self)
        
        self.loLambda = QVBoxLayout()
        self.lblLambda = QLabel("Longitud de onda [nm]", self)
        self.sldLambda = QSlider(Qt.Horizontal )
        self.sldLambda.setMinimum(400)
        self.sldLambda.setMaximum(700)
        self.sldLambda.setValue(700)
        self.sldLambda.setTickPosition(QSlider.TicksBelow)
        self.sldLambda.setTickInterval(5)
        self.edtLambda = QLineEdit(self)
        self.edtLambda.setMaxLength(5)
        self.loLambda.addWidget(self.lblLambda )
        self.loLambda.addSpacing(3)
        self.loLambda.addWidget(self.sldLambda )
        self.loLambda.addSpacing(3)
        self.loLambda.addWidget(self.edtLambda )
        
        self.loPhase = QVBoxLayout()
        self.lblPhase = QLabel("Fase Inicial [rad]", self)
        self.sldPhase = QSlider(Qt. Horizontal )
        self.sldPhase.setMinimum(0)
        self.sldPhase.setMaximum(2 * 4 * 100)
        self.sldPhase.setValue(0)
        self.sldPhase.setTickPosition(QSlider.TicksBelow )
        self.sldPhase.setTickInterval(1)
        self.edtPhase = QLineEdit(self)
        self.edtPhase.setMaxLength(5)
        self.loPhase.addWidget(self.lblPhase )
        self.loPhase.addSpacing(3)
        self.loPhase.addWidget(self.sldPhase )
        self.loPhase.addSpacing(3)
        self.loPhase.addWidget(self.edtPhase )
        
        self.loAmpl = QVBoxLayout()
        self.lblAmpl = QLabel("Amplitud", self)
        self.sldAmpl = QSlider(Qt. Horizontal )
        self.sldAmpl.setMinimum(10)
        self.sldAmpl.setMaximum(200)
        self.sldAmpl.setValue(100)
        self.sldAmpl.setTickPosition(QSlider.TicksBelow )
        self.sldAmpl.setTickInterval(1)
        self.edtAmpl = QLineEdit(self)
        self.edtAmpl.setMaxLength(5)
        self.loAmpl.addWidget(self.lblAmpl )
        self.loAmpl.addSpacing(3)
        self.loAmpl.addWidget(self.sldAmpl )
        self.loAmpl.addSpacing(3)
        self.loAmpl.addWidget(self.edtAmpl )
        
        self.loWaveParams = QHBoxLayout()
        self.loWaveParams.addLayout(self.loLambda )
        self.loWaveParams.addStretch()
        self.loWaveParams.addLayout(self.loPhase )
        self.loWaveParams.addStretch()
        self.loWaveParams.addLayout(self.loAmpl )


        lamda = self.sldLambda.value()
        phi = self.sldPhase.value()/100
        ampl = self.sldAmpl.value()/100
        self.edtLambda.setText(str(lamda))
        self.edtPhase.setText(str(phi))
        self.edtAmpl.setText(str(ampl))
        
        self.loCanvas = MplCanvas(
            self.main_widget,
            width = 5,
            height = 4,
            lamda = lamda,
            phi=phi,
            ampl=ampl
            )
        
        self.main_widget.setFocus()
        self.setCentralWidget(self.main_widget)
        
        self.loMaster = QVBoxLayout (self.main_widget )
        self.loMaster.addLayout (self.loWaveParams )
        self.loMaster.addWidget (self.loCanvas )
        
        
        self.sldLambda.valueChanged.connect(self.OnLambdaChanged )
        self.sldPhase.valueChanged.connect(self.OnPhaseChanged )
        self.sldAmpl.valueChanged.connect(self.OnAmplChanged )
        self.edtLambda.editingFinished.connect(self.OnEdtLambdaChanged )
        self.edtPhase.editingFinished.connect(self.OnEdtPhaseChanged )
        self.edtAmpl.editingFinished.connect(self.OnEdtAmplChanged )
        
    def OnLambdaChanged(self):
        lamda = self.sldLambda.value ()
        self.edtLambda.setText(str(lamda ))
        self.OnSomethingChanged ()
        
    def OnPhaseChanged(self):
        phi = self.sldPhase.value()/100
        self.edtPhase.setText(str(phi))
        self.OnSomethingChanged()
        
    def OnAmplChanged(self):
        ampl = self.sldAmpl.value()/100
        self.edtAmpl.setText(str(ampl))
        self.OnSomethingChanged()
        
    def OnEdtLambdaChanged(self):
        lamda = int(self.edtLambda.text())
        self.sldLambda.setValue(lamda)

    def OnEdtPhaseChanged(self):
        phi = self.edtPhase.text ()
        self.sldPhase.setValue(float(phi) * 100)

    def OnEdtAmplChanged(self):
        ampl = self.edtAmpl.text()
        self.sldAmpl.setValue(float(ampl) * 100)
                          
    def OnSomethingChanged (self):
        #usar ".cla()" para limpiar cada vez el grafico
        self.loCanvas.ax.cla()
        lamda = self.sldLambda.value ()
        phi = self.sldPhase.value()/100
        ampl = self.sldAmpl.value()/100
        self.loCanvas.drawPlot(lamda ,phi ,ampl)
        


if __name__ == "__main__":
    app = QApplication(sys.argv)
    miAplicacion = MainApp()
    miAplicacion.show()
    app.exec()
