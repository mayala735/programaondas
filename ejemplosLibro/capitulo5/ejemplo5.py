import sys
import matplotlib as mpl
mpl.use("Qt5Agg")
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from scipy import pi
from numpy import imag as np
from numpy import sin,cos,tan,arcsin,arctan,meshgrid,linspace ,sqrt      # se importan funciones en numpy, scipy will be deprecated
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FC
from matplotlib.figure import Figure
import matplotlib.lines as mlines
from matplotlib import pyplot as plt

def curva(p1, p2):
    import numpy as np
    a = (p2[1] - p1[1]) / ((p2[0]) - np.sin(p1[0]))             #   (yRef-0)/(sin(xRef)-0) originalmente
    #b = p1[1] - a * np.sin(p1[0])                              #   0 - yRef/sin(xRef) * sin(0), posiblemente innecesario
    xf = np.linspace(p1[0], p2[0], 500)
    yf = a * np.sin(xf*10)# + b

    m = 1#(p2[1]-p1[1])/(p1[0]-p1[0])

    #p1 = [0, 0]
    #p2 = [xReflRay, yReflRay]

    ang = arctan(m)

    print(cos(ang))         #ok

    x = xf*cos(ang)-yf*sin(ang)
    y = xf*cos(ang)+yf*sin(ang)

    return x, y

class MplCanvas(FC):
    def __init__(self,
                 parent=None,
                 width=8,
                 height=6.5,
                 nOne=1.0,
                 nTwo=1.52,
                 incAng=30
                 ):
        
        fig = Figure(figsize=(width, height ))
        self.ax = fig.add_subplot(111)
        #self.ax.hold(False)
        FC.__init__(self, fig )
        FC.setSizePolicy(self,
                         QSizePolicy.Expanding,
                         QSizePolicy.Expanding )
        FC.updateGeometry(self )
        self.drawPlot(nOne,
                      nTwo,
                      incAng)
        
        
    def drawPlot(self,
                 nOne,
                 nTwo,
                 incAng ):
        self.ax.clear()
        
        nOne = nOne/1000
        nTwo = nTwo/1000
        
        reflAng = incAng
        
        if -1 < nOne*sin(pi/180.0 * incAng )/nTwo < 1:
            refrAng = 180.0/pi * arcsin(nOne*sin(pi/180.0 * incAng )/nTwo )
        else:
            refrAng = sqrt((nOne*sin(pi/180.0 * incAng )/nTwo)*(nOne*sin(pi/180.0 * incAng )/nTwo))/(nOne*sin(pi/180.0 * incAng )/nTwo)
        
        x = linspace(-3.0,
                     3.0,
                     1024
                     )
        y = linspace(-3.0,
                     3.0,
                     1024
                     )
        
        Xc,Yc = meshgrid(x,
                       y)
        
        y = sin(2*pi*x/(nOne/1000) + nTwo)
        
        bdy = mlines.Line2D(
            [0, 0 ],
            [-3, 3 ],
            color = 'k'
            )
        self.ax.add_line(bdy)
        
        nml = mlines.Line2D(
            [-2 ,2 ],
            [0 ,0 ],
            ls='dashed',
            color = 'k'
            )
        self.ax.add_line(nml)
        
        lIncRay = lRefrRay = lReflRay = 2.8
        xIncRay = -lIncRay * cos(pi/180.0 * incAng )
        yIncRay = -lIncRay * sin(pi/180.0 * incAng )
        incRay = mlines.Line2D(
            [xIncRay, 0 ],
            [yIncRay, 0 ],
            color = 'k'
            )
        self.ax.add_line(incRay )
        self.ax.arrow(
            xIncRay/2,
            yIncRay/2,
            0.05,
            0.05 * tan(pi/180.0 * incAng ),
            length_includes_head = True,
            head_width =0.1,
            color = 'k',
            shape = 'full'
            )
                
        xReflRay = xIncRay
        yReflRay = -yIncRay
        reflRay = mlines.Line2D(
            [xReflRay,0 ],
            [yReflRay,0 ],
            color = 'k'
            )
        self.ax.add_line(reflRay )
        self.ax.arrow(
            xReflRay/2,
            yReflRay/2,
            -0.05,
            0.05 * tan(pi/180.0 * reflAng ),
            length_includes_head = True,
            head_width = 0.1,
            color = 'k',
            shape = 'full'
            )
    
        x0 = 1
        y0 = 1

        linea = mlines.Line2D(
            [0 ,x0 ],
            [0, y0 ],
            color = 'r'
        )
        #self.ax.add_line(linea )


        p1 = [0, 0]
        p2 = [xReflRay, yReflRay]
        x, y = curva(p1, p2)
        self.ax.plot(p1[0], p1[1], 'o')
        self.ax.plot(p2[0], p2[1], 'o')
        self.ax.plot(x, y)

        if not isinstance (refrAng, complex ):

            xRefrRay = lRefrRay * cos(pi/180.0 * refrAng )
            yRefrRay = lRefrRay * sin(pi/180.0 * refrAng )
            refrRay = mlines.Line2D(
                [0 , xRefrRay ],
                [0, yRefrRay ],
                color = 'k'
                )
            self.ax.add_line(refrRay )
            self.ax.arrow(
                xRefrRay/2,
                yRefrRay/2,
                0.05,
                0.05 * tan(pi/180.0 * refrAng ),
                length_includes_head = True,
                head_width = 0.1,
                color = 'k',
                shape = 'full'
                )
            
        self.ax.set_ylim(-3,3)
        self.ax.set_xlim(-3,3)
        self.ax.set_xticklabels([])
        self.ax.set_yticklabels([])
        self.ax.text(-2.5, 2.5 , 'Medio 1')
        self.ax.text (1.0 , 2.5 , 'Medio 2')
        self.draw()

class MainApp(QMainWindow ):
    def __init__(self):
        QMainWindow.__init__(self)
        
        self.setAttribute(Qt.WA_DeleteOnClose)
        self.setWindowTitle("Reflexion de ondas")
        
        self.main_widget = QWidget(self)
        
        self.lonOne = QVBoxLayout()
        self.lblnOne = QLabel("Indice de refraccion n1 %", self)
        self.sldnOne = QSlider(Qt.Horizontal )
        self.sldnOne.setMinimum(1)
        self.sldnOne.setMaximum(1000)
        self.sldnOne.setValue(1)
        self.sldnOne.setTickPosition(QSlider.TicksBelow)
        self.sldnOne.setTickInterval(5)
        self.edtnOne = QLineEdit(self)
        self.edtnOne.setMaxLength(5)
        self.lonOne.addWidget(self.lblnOne )
        self.lonOne.addSpacing(3)
        self.lonOne.addWidget(self.sldnOne )
        self.lonOne.addSpacing(3)
        self.lonOne.addWidget(self.edtnOne )
        
        self.lonTwo = QVBoxLayout()
        self.lblnTwo = QLabel("Indice de refraccion n2 %", self)
        self.sldnTwo = QSlider(Qt.Horizontal )
        self.sldnTwo.setMinimum(1)
        self.sldnTwo.setMaximum(1000)
        self.sldnTwo.setValue(1)
        self.sldnTwo.setTickPosition(QSlider.TicksBelow)
        self.sldnTwo.setTickInterval(5)
        self.edtnTwo = QLineEdit(self)
        self.edtnTwo.setMaxLength(5)
        self.lonTwo.addWidget(self.lblnTwo )
        self.lonTwo.addSpacing(3)
        self.lonTwo.addWidget(self.sldnTwo )
        self.lonTwo.addSpacing(3)
        self.lonTwo.addWidget(self.edtnTwo )
        
        self.loincAng = QVBoxLayout()
        self.lblincAng = QLabel("Ángulo de incidencia ", self)
        self.sldincAng = QSlider(Qt.Horizontal )
        self.sldincAng.setMinimum(0)
        self.sldincAng.setMaximum(90)
        self.sldincAng.setValue(45)
        self.sldincAng.setTickPosition(QSlider.TicksBelow)
        self.sldincAng.setTickInterval(5)
        self.edtincAng = QLineEdit(self)
        self.edtincAng.setMaxLength(5)
        self.loincAng.addWidget(self.lblincAng )
        self.loincAng.addSpacing(3)
        self.loincAng.addWidget(self.sldincAng )
        self.loincAng.addSpacing(3)
        self.loincAng.addWidget(self.edtincAng )
        
        self.loWaveParams = QHBoxLayout()
        self.loWaveParams.addLayout(self.lonOne )
        self.loWaveParams.addStretch()
        self.loWaveParams.addLayout(self.lonTwo )
        self.loWaveParams.addStretch()
        self.loWaveParams.addLayout(self.loincAng )


        fuenteSans = QFont("Mono Sans",15)
        self.lomaxAng = QVBoxLayout()
        self.lblmaxAngTxt = QLabel("Maximo angulo de incidencia", self)
        self.lblmaxAngTxt.setFont(fuenteSans)
        self.lblmaxAng = QLabel("incmax", self)
        self.lblmaxAng.setFont(fuenteSans)
        self.lomaxAng.addWidget(self.lblmaxAngTxt )
        self.lomaxAng.addWidget(self.lblmaxAng )

        self.lomaxnone = QVBoxLayout()
        self.lblmaxnoneTxt = QLabel("Maximo n1%", self)
        self.lblmaxnoneTxt.setFont(fuenteSans)
        self.lblmaxnone = QLabel("n1max", self)
        self.lblmaxnone.setFont(fuenteSans)
        self.lomaxnone.addWidget(self.lblmaxnoneTxt )
        self.lomaxnone.addWidget(self.lblmaxnone )

        self.lominntwo = QVBoxLayout()
        self.lblminntwoTxt = QLabel("Minimo n2%", self)
        self.lblminntwoTxt.setFont(fuenteSans)
        self.lblminntwo = QLabel("n2min", self)
        self.lblminntwo.setFont(fuenteSans)
        self.lominntwo.addWidget(self.lblminntwoTxt )
        self.lominntwo.addWidget(self.lblminntwo )
        
        self.lomaxParam = QHBoxLayout()
        self.lomaxParam.addLayout(self.lomaxnone )
        self.lomaxParam.addStretch()
        self.lomaxParam.addLayout(self.lominntwo )
        self.lomaxParam.addStretch()
        self.lomaxParam.addLayout(self.lomaxAng )
        
        
        nOne = self.sldnOne.value()
        nTwo = self.sldnTwo.value()
        incAng = self.sldincAng.value()
        
        self.edtnOne.setText(str(nOne))
        self.edtnTwo.setText(str(nTwo))
        self.edtincAng.setText(str(incAng))
        
        self.loCanvas = MplCanvas(
            self.main_widget,
            nOne=nOne,
            nTwo=nTwo,
            incAng=incAng
            )
        
        self.main_widget.setFocus()
        self.setCentralWidget(self.main_widget)
        
        self.loMaster = QVBoxLayout (self.main_widget )
        self.loMaster.addLayout (self.loWaveParams )
        self.loMaster.addWidget (self.loCanvas )
        self.loMaster.addLayout(self.lomaxParam )
        
        
        self.sldnOne.valueChanged.connect(self.OnnOneChanged )
        self.sldnTwo.valueChanged.connect(self.OnnTwoChanged )
        self.sldincAng.valueChanged.connect(self.OnincAngChanged )
        
        self.edtnOne.editingFinished.connect(self.OnEdtnOneChanged )
        self.edtnTwo.editingFinished.connect(self.OnEdtnTwoChanged )
        self.edtincAng.editingFinished.connect(self.OnEdtincAngChanged )
        
    def OnnOneChanged(self):
        nOne = self.sldnOne.value ()
        self.edtnOne.setText(str(nOne ))
        self.OnSomethingChanged ()
    
    def OnnTwoChanged(self):
        nTwo = self.sldnTwo.value ()
        self.edtnTwo.setText(str(nTwo ))
        self.OnSomethingChanged ()
    
    def OnincAngChanged(self):
        incAng = self.sldincAng.value ()
        self.edtincAng.setText(str(incAng ))
        self.OnSomethingChanged ()
        
    def OnEdtnOneChanged(self):
        nOne = int(self.edtnOne.text())
        self.sldnOne.setValue(nOne)

    def OnEdtnTwoChanged(self):
        nTwo = int(self.edtnTwo.text())
        self.sldnTwo.setValue(nTwo)

    def OnEdtincAngChanged(self):
        incAng = int(self.edtincAng.text())
        self.sldincAng.setValue(incAng)

    def OnSomethingChanged (self):
        #usar ".cla()" para limpiar cada vez el grafico
        self.loCanvas.ax.cla()
        nOne = self.sldnOne.value ()
        nTwo = self.sldnTwo.value ()
        incAng = self.sldincAng.value ()
        self.loCanvas.drawPlot(nOne ,nTwo ,incAng)

        if sin(incAng):
            maxnone = nTwo/sin(incAng*pi/180)
            self.lblmaxnone.setText(str(round(maxnone,2)))
        else:
            self.lblmaxnone.setText("Inf.")

        minntwo = nOne*sin(incAng*pi/180)

        if -1 < nTwo/nOne < 1:
            maxAngle = (arcsin(nTwo/nOne)) * 180/pi
        else:
            maxAngle = -1   #incorrecto
        
        self.lblminntwo.setText(str(round(minntwo,2)))
        if maxAngle.imag:
            self.lblmaxAng.setText("No existe")
        else:
            self.lblmaxAng.setText(str(round(maxAngle,2)))

        
        
if __name__ == "__main__":
    app = QApplication(sys.argv)
    miAplicacion = MainApp()
    miAplicacion.show()
    app.exec()