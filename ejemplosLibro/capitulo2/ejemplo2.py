from PyQt5. QtWidgets import QApplication , QMainWindow
import sys

class MainWindow(QMainWindow):
    def __init__(self):
        super(MainWindow, self).__init__()
        
        self.setGeometry(400, 100, 300, 200)
        self.setWindowTitle('Prueba')
        

if __name__ == "__main__":
    app = QApplication(sys.argv)
    miAplicacion = MainWindow()
    miAplicacion.show()
    app.exec_()
