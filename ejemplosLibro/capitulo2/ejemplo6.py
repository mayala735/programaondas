import sys
from PyQt5.QtWidgets import (QApplication , QMainWindow , QLabel ,
QPushButton , QSpinBox , QSlider )
from PyQt5.QtGui import QPixmap
from PyQt5.QtCore import pyqtSlot, Qt
c =299792458

class MainWindow(QMainWindow):
    
    def __init__(self):
        super(MainWindow, self).__init__()
        self.setGeometry(400, 100, 300, 200)
        self.setWindowTitle('Mi aplicacion')
        
        label1 = QLabel('Texto LABEL 1:', self)
        label1.setFixedWidth(120)
        label1.move(10, 10)
        
        
        self.label2 = QLabel("", self)
        mensaje = "<h3 ><b><font color=’green ’>Hello Python !</font ></b>"
        self.label2.setText(mensaje)
        self.label2.setFixedWidth(120)
        self.label2.move(130, 10)
        
        
        self.label3 = QLabel("", self)
        self.label3.setPixmap(QPixmap("greating.svg"))
        self.label3.setFixedSize(500, 500)
        self.label3.show()
        self.label3.move(10, 10)
        
        boton1 = QPushButton('Hola', self)
        boton1.setToolTip('Este botón te dice hola')
        boton1.move(50, 50)
        boton1.clicked.connect(self.on_click_boton1)
        
        boton2 = QPushButton('Chau', self)
        boton2.setToolTip('Este botón te dice chau')
        boton2.move(170, 50)
        boton2.clicked.connect(self.on_click_boton2)
        
        self.spb = QSpinBox(self)
        self.spb.setMinimum(0)
        self.spb.setMaximum(100)
        self.spb.setValue(50)
        self.spb.setSingleStep(1)
        self.spb.setGeometry(80, 110, 50, 20)
        self.spb.valueChanged.connect(self.spb_cambiarValor)
        
        txt = QLabel('El valor actual es:', self)
        txt.setGeometry(10, 130, 125, 20)
        self.val = QLabel("",self)
        self.val.setGeometry(130, 130, 25, 20)
        
        self.sldr = QSlider(Qt.Horizontal, self)
        self.sldr.setMinimum(0)
        self.sldr.setMaximum(100)
        self.sldr.setValue(50)
        self.sldr.setSingleStep(1)
        self.sldr.setGeometry(75, 150, 150, 20)
        self.sldr.valueChanged.connect(self.sldr_cambiarValor)
        
        
    @pyqtSlot()
    def on_click_boton1(self):
        mensaje ="<h3 ><b><font color=’green ’>HOLAAAA :)</font ></b>"
        self.label2.setText(mensaje)
        self.label2.setFixedWidth(120)
    
    @pyqtSlot()
    def on_click_boton2(self):
        mensaje ="<h3 ><b><font color=’green ’>CHAUUUU :)</font ></b>"
        self.label2.setText(mensaje)
        self.label2.setFixedWidth(120)
    
    @pyqtSlot(int)
    def spb_cambiarValor(self, valor):
        self.val.setText(str(self.spb.value()))
        self.sldr.setValue(valor)
        
    @pyqtSlot(int)
    def sldr_cambiarValor(self, valor):
        self.val.setText(str(self.spb.value()))
        self.spb.setValue(valor)


if __name__ == "__main__":
    app = QApplication(sys.argv)
    miAplicacion = MainWindow()
    miAplicacion.show()
    app.exec_()
    
