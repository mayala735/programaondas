import sys
from PyQt5. QtWidgets import QApplication , QMainWindow , QLabel
from PyQt5.QtGui import QPixmap

class MainWindow(QMainWindow):
    
    def __init__(self):
        super(MainWindow, self).__init__()
        self.setGeometry(400, 100, 300, 200)
        self.setWindowTitle('Mi aplicacion')
        
        label1 = QLabel('Texto LABEL 1:', self)
        label1.setFixedWidth(120)
        label1.move(10, 10)
        
        
        label2 = QLabel("", self)
        mensaje = "<h3 ><b><font color=’green ’>Hello Python !</font ></b>"
        label2.setText(mensaje)
        label2.setFixedWidth(120)
        label2.move(130, 10)
        
        
        label3 = QLabel("", self)
        label3.setPixmap(QPixmap("greating.svg"))
        label3.setFixedSize(500, 500)
        label3.show()
        label3.move(10, 10)
        

if __name__ == "__main__":
    app = QApplication(sys.argv)
    miAplicacion = MainWindow()
    miAplicacion.show()
    app.exec_()
    
