# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'manejoArchivoTexto.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(517, 398)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.textoCambiante = QtWidgets.QLabel(self.centralwidget)
        self.textoCambiante.setGeometry(QtCore.QRect(20, 30, 211, 61))
        font = QtGui.QFont()
        font.setFamily("Tibetan Machine Uni")
        font.setPointSize(30)
        self.textoCambiante.setFont(font)
        self.textoCambiante.setObjectName("textoCambiante")
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 517, 22))
        self.menubar.setObjectName("menubar")
        self.menuArchivo = QtWidgets.QMenu(self.menubar)
        self.menuArchivo.setObjectName("menuArchivo")
        self.menuEditar = QtWidgets.QMenu(self.menubar)
        self.menuEditar.setObjectName("menuEditar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)
        self.actionNuevo = QtWidgets.QAction(MainWindow)
        self.actionNuevo.setObjectName("actionNuevo")
        self.actionGuardar = QtWidgets.QAction(MainWindow)
        self.actionGuardar.setObjectName("actionGuardar")
        self.actionCopiar = QtWidgets.QAction(MainWindow)
        self.actionCopiar.setObjectName("actionCopiar")
        self.actionPegar = QtWidgets.QAction(MainWindow)
        self.actionPegar.setObjectName("actionPegar")
        self.menuArchivo.addAction(self.actionNuevo)
        self.menuArchivo.addAction(self.actionGuardar)
        self.menuEditar.addAction(self.actionCopiar)
        self.menuEditar.addAction(self.actionPegar)
        self.menubar.addAction(self.menuArchivo.menuAction())
        self.menubar.addAction(self.menuEditar.menuAction())

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

        self.actionNuevo.triggered.connect(lambda: self.clickear("Nuevo archivo"))
        self.actionGuardar.triggered.connect(lambda: self.clickear("Guardo archivo"))
        self.actionCopiar.triggered.connect(lambda: self.clickear("Copio archivo"))
        self.actionPegar.triggered.connect(lambda: self.clickear("Pego archivo"))
        
    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.textoCambiante.setText(_translate("MainWindow", "TextLabel"))
        self.menuArchivo.setTitle(_translate("MainWindow", "Archivo"))
        self.menuEditar.setTitle(_translate("MainWindow", "Editar"))
        self.actionNuevo.setText(_translate("MainWindow", "Nuevo"))
        self.actionNuevo.setStatusTip(_translate("MainWindow", "Crear un nuevo texto"))
        self.actionNuevo.setShortcut(_translate("MainWindow", "Ctrl+N"))
        self.actionGuardar.setText(_translate("MainWindow", "Guardar"))
        self.actionGuardar.setStatusTip(_translate("MainWindow", "Guardar el texto"))
        self.actionGuardar.setShortcut(_translate("MainWindow", "Ctrl+G"))
        self.actionCopiar.setText(_translate("MainWindow", "Copiar"))
        self.actionCopiar.setStatusTip(_translate("MainWindow", "Copiar texto"))
        self.actionCopiar.setShortcut(_translate("MainWindow", "Ctrl+C"))
        self.actionPegar.setText(_translate("MainWindow", "Pegar"))
        self.actionPegar.setStatusTip(_translate("MainWindow", "Pegar el texto"))
        self.actionPegar.setShortcut(_translate("MainWindow", "Ctrl+V"))
    
    def clickear(self, text):
        self.textoCambiante.setText(text)
        self.textoCambiante.adjustSize()
    

if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())

