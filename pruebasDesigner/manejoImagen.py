# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'manejoImagen.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(509, 396)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.imagen = QtWidgets.QLabel(self.centralwidget)
        self.imagen.setGeometry(QtCore.QRect(80, 0, 351, 251))
        self.imagen.setText("")
        self.imagen.setPixmap(QtGui.QPixmap("wa.jpg"))
        self.imagen.setScaledContents(True)
        self.imagen.setObjectName("imagen")
        self.wa = QtWidgets.QPushButton(self.centralwidget)
        self.wa.setGeometry(QtCore.QRect(80, 290, 89, 25))
        self.wa.setObjectName("wa")
        self.girador = QtWidgets.QPushButton(self.centralwidget)
        self.girador.setGeometry(QtCore.QRect(80, 320, 89, 25))
        self.girador.setObjectName("girador")
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 509, 22))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

        self.wa.clicked.connect(self.mostrar_wa)
        self.girador.clicked.connect(self.mostrar_girador)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.wa.setText(_translate("MainWindow", "wa"))
        self.girador.setText(_translate("MainWindow", "girador"))
    
    def mostrar_wa(self):
        self.imagen.setPixmap(QtGui.QPixmap("wa.jpg"))
        
    def mostrar_girador(self):
        self.imagen.setPixmap(QtGui.QPixmap("girador.png"))

if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())

